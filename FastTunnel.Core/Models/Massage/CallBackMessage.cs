// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastTunnel.Core.Models
{
    /// <summary>
    /// webSocket消息返回
    /// </summary>
    public class CallBackMessage
    {
        /// <summary>
        /// 操作
        /// </summary>
        public OperationEnum Operation { get; set; }

        /// <summary>
        /// 配置类型
        /// </summary>
        public ConfigTypeEnum ConfigType { get; set; }

        /// <summary>
        /// 本地IP
        /// </summary>
        public string LocalIp { get; set; }

        /// <summary>
        /// 本地端口
        /// </summary>
        public int LocalPort { get; set; }

        /// <summary>
        /// 结果
        /// </summary>
        public string Result { get; set; }
    }

    /// <summary>
    /// 操作类型
    /// </summary>
    public enum OperationEnum
    {
        Add = 1, Delete
    }

    public enum ConfigTypeEnum
    {
        ForWord = 1, Web
    }
}
