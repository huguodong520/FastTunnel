// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FastTunnel.Core.Client;
using FastTunnel.Core.Config;
using FastTunnel.Core.Extensions;
using FastTunnel.Core.Handlers.Client;
using FastTunnel.Core.Models;
using FastTunnel.Core.Utilitys;
using FastTunnel.Core.WebClientSocket;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FastTunnel.Core.WebClientSocket
{
    /// <summary>
    /// 连接websockt
    /// </summary>
    public class FastClientSocket : IFastClientSocket
    {
        public readonly ClientWebSocket socket;

        protected readonly ILogger<FastClientSocket> _logger;
        public DefaultClientConfig ClientConfig { get; private set; }
        private readonly SwapHandler _newCustomerHandler;
        private readonly CallBackHandle _callBackHandle;

        public SuiDaoServer Server { get; protected set; }

        private readonly LogHandler _logHandler;
        public FastClientSocket(ILogger<FastClientSocket> logger,
             SwapHandler newCustomerHandler,
             CallBackHandle callBackHandle,
              LogHandler logHandler,
            IOptionsMonitor<DefaultClientConfig> configuration)
        {
            _logger = logger;
            _newCustomerHandler = newCustomerHandler;
            this._callBackHandle = callBackHandle;
            _logHandler = logHandler;
            ClientConfig = configuration.CurrentValue;
            Server = ClientConfig.Server;
            socket = new ClientWebSocket();
            socket.Options.RemoteCertificateValidationCallback = delegate { return true; };
            socket.Options.SetRequestHeader(FastTunnelConst.FASTTUNNEL_VERSION, AssemblyUtility.GetVersion().ToString());
            socket.Options.SetRequestHeader(FastTunnelConst.FASTTUNNEL_TOKEN, ClientConfig.Token);
        }

        /// <summary>
        /// 连接服务端
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task LoginAsync(CancellationToken cancellationToken)
        {
            try
            {
                //var logMsg = GetLoginMsg(cancellationToken);

                // 连接到的目标IP
                _logger.LogInformation($"正在连接服务端 {Server.ServerAddr}:{Server.ServerPort}");
                await socket.ConnectAsync(
                    new Uri($"{Server.Protocol}://{Server.ServerAddr}:{Server.ServerPort}"), cancellationToken);
                _logger.LogInformation("连接服务端成功");

                // 登录
                //await socket.SendCmdAsync(MessageType.LogIn, logMsg, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError($"连接服务端失败：{ex.Message}", ex);
                throw;
            }

        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="msg"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task SendCmdAsync(MessageType messageType, string msg, CancellationToken cancellationToken)
        {
            try
            {
                await socket.SendCmdAsync(messageType, msg, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError("SendCmdAsync失败", ex);
                throw;
            }

        }

        /// <summary>
        /// 接收消息
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task ReceiveServerAsync(CancellationToken cancellationToken, FastTunnelWebClient client)
        {
            byte[] buffer = new byte[FastTunnelConst.MAX_CMD_LENGTH];
            while (!cancellationToken.IsCancellationRequested)
            {
                var res = await socket.ReceiveAsync(buffer, cancellationToken);
                var type = buffer[0];
                var content = Encoding.UTF8.GetString(buffer, 1, res.Count - 1);
                HandleServerRequestAsync(type, content, cancellationToken, client);
            }
        }

        private async void HandleServerRequestAsync(byte cmd, string ctx, CancellationToken cancellationToken, FastTunnelWebClient client)
        {
            await Task.Yield();

            try
            {
                IClientHandler handler;
                switch ((MessageType)cmd)
                {
                    case MessageType.SwapMsg:
                    case MessageType.Forward:
                        handler = _newCustomerHandler;
                        break;
                    case MessageType.Log:
                        handler = _logHandler;
                        break;
                    case MessageType.CallBack:
                        handler = _callBackHandle;
                        break;
                    default:
                        throw new Exception($"未处理的消息：cmd={cmd}");
                }

                await handler.HandlerMsgAsync(client, ctx, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError("处理消息异常", ex);
            }
        }
    }
}
