// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FastTunnel.Core.Client;
using FastTunnel.Core.Models;

namespace FastTunnel.Core.WebClientSocket
{
    /// <summary>
    /// 连接websockt
    /// </summary>
    public interface IFastClientSocket
    {
        /// <summary>
        /// 连接服务端
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task LoginAsync(CancellationToken cancellationToken);

        /// <summary>
        /// 接收消息
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        Task ReceiveServerAsync(CancellationToken cancellationToken, FastTunnelWebClient client);

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="msg"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task SendCmdAsync(MessageType messageType, string msg, CancellationToken cancellationToken);
    }
}
