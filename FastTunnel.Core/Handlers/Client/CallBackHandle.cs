// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FastTunnel.Core.Client;
using FastTunnel.Core.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FastTunnel.Core.Handlers.Client
{
    public class CallBackHandle : IClientHandler
    {
        ILogger<CallBackHandle> _logger;
        private readonly IMemoryCache _memoryCache;
        public CallBackHandle(ILogger<CallBackHandle> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            this._memoryCache = memoryCache;
        }

        public async Task HandlerMsgAsync(FastTunnelClient cleint, string msg, CancellationToken cancellationToken)
        {
            var message = JsonConvert.DeserializeObject<CallBackMessage>(msg);
            var key = $"{message.LocalIp}:{message.LocalPort}:{(int)message.ConfigType}:{(int)message.Operation}";
            _memoryCache.GetOrCreate(key, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromSeconds(60);
                return message;
            });
            await Task.CompletedTask;
        }
    }
}
