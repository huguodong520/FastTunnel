// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using FastTunnel.Core.Client;
using FastTunnel.Core.Dispatchers;
using FastTunnel.Core.Extensions;
using FastTunnel.Core.Listener;
using FastTunnel.Core.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;

using System.Threading;
using System.Threading.Tasks;
using Yarp.ReverseProxy.Configuration;
using Yarp.Sample;


namespace FastTunnel.Core.Handlers.Server
{
    public class LoginHandler : ILoginHandler
    {
        readonly ILogger logger;
        readonly IProxyConfigProvider proxyConfig;
        public const bool NeedRecive = true;

        public LoginHandler(ILogger<LoginHandler> logger, IProxyConfigProvider proxyConfig)
        {
            this.proxyConfig = proxyConfig;
            this.logger = logger;
        }

        protected async Task HandleLoginAsync(FastTunnelServer server, TunnelClient client, LogInMassage requet)
        {
            bool hasTunnel = false;

            await client.webSocket.SendCmdAsync(MessageType.Log, $"穿透协议 | 映射关系（公网=>内网）", CancellationToken.None);
            Thread.Sleep(300);

            if (requet.Webs != null && requet.Webs.Any())
            {
                hasTunnel = true;
                foreach (var item in requet.Webs)
                {
                    var hostName = $"{item.SubDomain}.{server.ServerOption.CurrentValue.WebDomain}".Trim().ToLower();
                    var info = new WebInfo { Socket = client.webSocket, WebConfig = item };

                    logger.LogDebug($"new domain '{hostName}'");
                    server.WebList.AddOrUpdate(hostName, info, (key, oldInfo) => { return info; });
                    (proxyConfig as InMemoryConfigProvider).AddWeb(hostName);

                    await client.webSocket.SendCmdAsync(MessageType.Log, $"  HTTP   | http://{hostName}:{client.ConnectionPort} => {item.LocalIp}:{item.LocalPort}", CancellationToken.None);
                    client.AddWeb(info);

                    if (item.WWW != null)
                    {
                        foreach (var www in item.WWW)
                        {
                            // TODO:validateDomain
                            hostName = www.Trim().ToLower();
                            server.WebList.AddOrUpdate(www, info, (key, oldInfo) => { return info; });
                            (proxyConfig as InMemoryConfigProvider).AddWeb(www);

                            await client.webSocket.SendCmdAsync(MessageType.Log, $"  HTTP   | http://{www}:{client.ConnectionPort} => {item.LocalIp}:{item.LocalPort}", CancellationToken.None);
                            client.AddWeb(info);
                        }
                    }
                }
            }

            if (requet.Forwards != null && requet.Forwards.Any())
            {
                if (server.ServerOption.CurrentValue.EnableForward)
                {
                    hasTunnel = true;

                    foreach (var item in requet.Forwards)
                    {
                        try
                        {
                            ForwardInfo<ForwardHandlerArg> old;
                            if (server.ForwardList.TryGetValue(item.RemotePort, out old))
                            {
                                logger.LogDebug($"Remove Listener {old.Listener.ListenIp}:{old.Listener.ListenPort}");
                                old.Listener.Stop();
                                server.ForwardList.TryRemove(item.RemotePort, out ForwardInfo<ForwardHandlerArg> _);
                            }

                            // TODO: 客户端离线时销毁
                            var ls = new PortProxyListener("0.0.0.0", item.RemotePort, logger, client.webSocket);
                            ls.Start(new ForwardDispatcher(logger, server, item));



                            var forwardInfo = new ForwardInfo<ForwardHandlerArg> { Listener = ls, Socket = client.webSocket, SSHConfig = item };

                            // TODO: 客户端离线时销毁
                            server.ForwardList.TryAdd(item.RemotePort, forwardInfo);
                            logger.LogDebug($"SSH proxy success: {item.RemotePort} => {item.LocalIp}:{item.LocalPort}");

                            client.AddForward(forwardInfo);
                            await client.webSocket.SendCmdAsync(MessageType.Log, $"  TCP    | {server.ServerOption.CurrentValue.WebDomain}:{item.RemotePort} => {item.LocalIp}:{item.LocalPort}", CancellationToken.None);
                        }
                        catch (Exception ex)
                        {
                            logger.LogError($"SSH proxy error: {item.RemotePort} => {item.LocalIp}:{item.LocalPort}");
                            logger.LogError(ex.Message);
                            await client.webSocket.SendCmdAsync(MessageType.Log, ex.Message, CancellationToken.None);
                            continue;
                        }
                    }
                }
                else
                {
                    await client.webSocket.SendCmdAsync(MessageType.Log, TunnelResource.ForwardDisabled, CancellationToken.None);
                }
            }

            if (!hasTunnel)
                await client.webSocket.SendCmdAsync(MessageType.Log, TunnelResource.NoTunnel, CancellationToken.None);
        }
        /// <summary>
        /// 更新节点
        /// </summary>
        /// <param name="server"></param>
        /// <param name="client"></param>
        /// <param name="requet"></param>
        /// <returns></returns>
        protected async Task HandleLoginUpdateAsync(FastTunnelServer server, TunnelClient client, LogInMassage requet)
        {

            bool hasTunnel = false;

            await client.webSocket.SendCmdAsync(MessageType.Log, $"收到更新节点请求", CancellationToken.None);
            Thread.Sleep(100);
            if (requet.Webs != null && requet.Webs.Any())
            {
                hasTunnel = true;
                foreach (var item in requet.Webs)
                {
                    CallBackMessage callBack = new CallBackMessage()
                    {
                        Operation = OperationEnum.Delete,
                        ConfigType = ConfigTypeEnum.Web,
                        LocalIp = item.LocalIp,
                        LocalPort = item.LocalPort
                    };
                    var hostName = $"{item.SubDomain}.{server.ServerOption.CurrentValue.WebDomain}".Trim().ToLower();
                    var info = new WebInfo { Socket = client.webSocket, WebConfig = item };
                    WebInfo old;
                    if (server.WebList.TryGetValue(hostName, out old))
                    {
                        logger.LogDebug($"移除节点 '{hostName}'");
                        server.WebList.TryRemove(hostName, out WebInfo _);
                    }
                    if (!item.IsDelete)//新增
                    {
                        logger.LogDebug($"new domain '{hostName}'");
                        server.WebList.AddOrUpdate(hostName, info, (key, oldInfo) => { return info; });
                        (proxyConfig as InMemoryConfigProvider).AddWeb(hostName);
                        client.AddWeb(info);
                        callBack.Operation = OperationEnum.Add;
                        callBack.Result = $"http://{hostName}:{client.ConnectionPort} => {item.LocalIp}:{item.LocalPort}";

                    }
                    var msg = JsonConvert.SerializeObject(callBack);
                    await client.webSocket.SendCmdAsync(MessageType.CallBack, msg, CancellationToken.None);

                }
            }

            if (requet.Forwards != null && requet.Forwards.Any())
            {
                if (server.ServerOption.CurrentValue.EnableForward)
                {
                    hasTunnel = true;

                    foreach (var item in requet.Forwards)
                    {

                        try
                        {
                            CallBackMessage callBack = new CallBackMessage()
                            {
                                Operation = OperationEnum.Delete,
                                ConfigType = ConfigTypeEnum.ForWord,
                                LocalIp = item.LocalIp,
                                LocalPort = item.LocalPort
                            };
                            ForwardInfo<ForwardHandlerArg> old;
                            if (server.ForwardList.TryGetValue(item.RemotePort, out old))
                            {
                                logger.LogDebug($"移除监听: {old.Listener.ListenIp}:{old.Listener.ListenPort}");
                                old.Listener.Stop();
                                server.ForwardList.TryRemove(item.RemotePort, out ForwardInfo<ForwardHandlerArg> _);
                            }
                            if (!item.IsDelete)//表示新增
                            {
                                // TODO: 客户端离线时销毁
                                var ls = new PortProxyListener("0.0.0.0", item.RemotePort, logger, client.webSocket);
                                ls.Start(new ForwardDispatcher(logger, server, item));

                                var forwardInfo = new ForwardInfo<ForwardHandlerArg> { Listener = ls, Socket = client.webSocket, SSHConfig = item };

                                // TODO: 客户端离线时销毁
                                server.ForwardList.TryAdd(item.RemotePort, forwardInfo);
                                logger.LogDebug($"代理成功: {item.RemotePort} => {item.LocalIp}:{item.LocalPort}");

                                client.AddForward(forwardInfo);
                                callBack.Operation = OperationEnum.Add;
                                callBack.Result = $"{server.ServerOption.CurrentValue.WebDomain}:{item.RemotePort} => {item.LocalIp}:{item.LocalPort}";

                            }
                            var msg = JsonConvert.SerializeObject(callBack);
                            await client.webSocket.SendCmdAsync(MessageType.CallBack, msg, CancellationToken.None);
                        }
                        catch (Exception ex)
                        {
                            logger.LogError($"HandleLoginUpdateAsync失败: {item.RemotePort} => {item.LocalIp}:{item.LocalPort}");
                            logger.LogError(ex.Message);
                            await client.webSocket.SendCmdAsync(MessageType.Log, ex.Message, CancellationToken.None);
                            continue;
                        }
                    }
                }


            }
        }
        public virtual async Task<bool> HandlerMsg(FastTunnelServer fastTunnelServer, TunnelClient tunnelClient, string lineCmd)
        {
            var msg = JsonConvert.DeserializeObject<LogInMassage>(lineCmd);

            if (!msg.IsUpdate)//是否要更新
            {
                await HandleLoginAsync(fastTunnelServer, tunnelClient, msg);
            }
            else
            {
                await HandleLoginUpdateAsync(fastTunnelServer, tunnelClient, msg);
            }
            return NeedRecive;
        }
    }
}
