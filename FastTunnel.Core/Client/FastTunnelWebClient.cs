// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FastTunnel.Core.Config;
using FastTunnel.Core.Extensions;
using FastTunnel.Core.Handlers.Client;
using FastTunnel.Core.Models;
using FastTunnel.Core.WebClientSocket;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FastTunnel.Core.Client
{
    public class FastTunnelWebClient : FastTunnelClient
    {
        protected readonly ILogger<FastTunnelWebClient> _logger;
        private readonly SwapHandler _newCustomerHandler;
        private readonly LogHandler _logHandler;
        private readonly IFastClientSocket _clientSocket;

        public DefaultClientConfig ClientConfig { get; private set; }

        public SuiDaoServer Server { get; protected set; }

        public WebManager WebManager { get; protected set; }

        private readonly SqlSugar.SqlSugarScope _db;
        public FastTunnelWebClient(ILogger<FastTunnelWebClient> logger,
                                   SwapHandler newCustomerHandler,
                                   LogHandler logHandler,
                                   IOptionsMonitor<DefaultClientConfig> configuration,
                                   IFastClientSocket clientSocket)
            : base(logger, newCustomerHandler, logHandler, configuration)
        {
            _logger = logger;
            _newCustomerHandler = newCustomerHandler;
            _logHandler = logHandler;
            this._clientSocket = clientSocket;
            ClientConfig = configuration.CurrentValue;
            Server = ClientConfig.Server;
            WebManager = ClientConfig.WebManager;
        }

        public override async void StartAsync(CancellationToken cancellationToken)
        {
            //await newCustomerHandler.HandlerMsgAsync(this, "", cancellationToken);
            //base.StartAsync(cancellationToken);

            _logger.LogInformation("===== FastTunnel Client Start =====");

            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    var forwards = new List<ForwardConfig>();
                    var webs = new List<WebConfig>();
                    //获取Web端的端口
                    var webPort = WebManager.Port;
                    //查找web配置是否配置了端口
                    var web = ClientConfig.Webs.Where(it => it.LocalPort == webPort).FirstOrDefault();
                    if (web != null)
                    {
                        webs.Add(web);
                    }
                    else
                    {
                        //查找forward配置是否配置了端口
                        var forward = ClientConfig.Forwards.Where(it => it.LocalPort == webPort).FirstOrDefault();
                        if (forward != null)
                        {
                            forwards.Add(forward);
                        }
                        else
                        {
                            throw new Exception("未配置web管理页面端口");
                        }
                    }


                    await _clientSocket.LoginAsync(cancellationToken);//连接到服务端
                    var msg = new LogInMassage
                    {
                        Webs = webs,
                        Forwards = forwards,
                    }.ToJson();
                    await _clientSocket.SendCmdAsync(Models.MessageType.LogIn, msg, cancellationToken);
                    await _clientSocket.ReceiveServerAsync(cancellationToken, this);//接收服务端消息

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    await Task.Delay(TimeSpan.FromSeconds(10), cancellationToken);
                }
            }

            _logger.LogInformation("===== FastTunnel Client End =====");

        }


    }
}
