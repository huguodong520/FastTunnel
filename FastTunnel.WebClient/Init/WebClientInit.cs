// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using FastTunnel.Core.Extensions;
using FastTunnel.Core.Models;
using FastTunnel.Core.WebClientSocket;
using Mapster;

namespace FastTunnel.WebClient;

public class WebClientInit : IHostedService
{
    private readonly ILogger<WebClientInit> _logger;
    private readonly IFastClientSocket clientSocket;
    private readonly SqlSugar.SqlSugarScope _db;
    public WebClientInit(ILogger<WebClientInit> logger, IFastClientSocket clientSocket)
    {
        this._logger = logger;
        this.clientSocket = clientSocket;
        _db = DbContext.Db;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var client = await _db.Queryable<Client>().Where(it => it.IsOpen == true).ToListAsync();
        var forwards = client.Where(it => it.ConfigType == 1).ToList().Adapt<List<ForwardConfig>>();
        var webs = client.Where(it => it.ConfigType == 2).ToList().Adapt<List<WebConfig>>();
        var msg = new LogInMassage
        {
            Webs = webs,
            Forwards = forwards,
        }.ToJson();
        await clientSocket.SendCmdAsync(MessageType.LogIn, msg, cancellationToken);
        await Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}
