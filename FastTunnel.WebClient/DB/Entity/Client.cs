// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System;
using FastTunnel.Core.Models;

namespace FastTunnel.WebClient;
/// <summary>
/// 客户端表
///</summary>
[SugarTable("Client")]
public class Client
{
    /// <summary>
    /// Id 
    ///</summary>
    [SugarColumn(ColumnName = "Id", IsPrimaryKey = true, IsIdentity = true)]
    public int Id { get; set; }
    /// <summary>
    /// 配置名称 
    ///</summary>
    [SugarColumn(ColumnName = "Name")]
    public string Name { get; set; }
    /// <summary>
    /// 配置类型 
    ///</summary>
    [SugarColumn(ColumnName = "ConfigType")]
    public int ConfigType { get; set; }
    /// <summary>
    /// 内网IP 
    ///</summary>
    [SugarColumn(ColumnName = "LocalIp")]
    public string LocalIp { get; set; }
    /// <summary>
    /// 内网端口 
    ///</summary>
    [SugarColumn(ColumnName = "LocalPort")]
    public int LocalPort { get; set; }
    /// <summary>
    /// 服务端端口 
    ///</summary>
    [SugarColumn(ColumnName = "ServerPort")]
    public int RemotePort { get; set; }
    /// <summary>
    /// 通讯协议 
    ///</summary>
    [SugarColumn(ColumnName = "Protocol")]
    public ProtocolEnum Protocol { get; set; }
    /// <summary>
    /// 子域名 
    ///</summary>
    [SugarColumn(ColumnName = "SubDomain")]
    public string SubDomain { get; set; }
    /// <summary>
    /// 生成的结果字符串预览 
    ///</summary>
    [SugarColumn(ColumnName = "Result")]
    public string Result { get; set; }
    /// <summary>
    /// 是否启用 
    ///</summary>
    [SugarColumn(ColumnName = "IsOpen")]
    public bool IsOpen { get; set; }
    /// <summary>
    /// 更新时间 
    ///</summary>
    [SugarColumn(ColumnName = "UpdatedTime")]
    public DateTime UpdatedTime { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [SugarColumn(ColumnName = "Remark")]
    public string Remark { get; set; }
}

