using Furion;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastTunnel.WebClient
{

    public class DbContext
    {
        public static SqlSugarScope Db = new SqlSugarScope(new ConnectionConfig()
        {
            DbType = SqlSugar.DbType.Sqlite,
            ConnectionString = App.Configuration["ConnectionStrings:Sqlite"],//获取连接字符串
            IsAutoCloseConnection = true
        },
         db =>
         {
         //单例参数配置，所有上下文生效
         db.Aop.OnLogExecuting = (sql, pars) =>
               {
               //if (sql.StartsWith("SELECT"))
               //{
               //    Console.ForegroundColor = ConsoleColor.Green;
               //}
               //if (sql.StartsWith("UPDATE") || sql.StartsWith("INSERT"))
               //{
               //    Console.ForegroundColor = ConsoleColor.White;
               //}  
               //if (sql.StartsWith("DELETE"))
               //{
               //    Console.ForegroundColor = ConsoleColor.Blue;
               //}
               //App.PrintToMiniProfiler("SqlSugar", "Info", sql + "\r\n" + db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
               //Console.WriteLine(sql + "\r\n\r\n" + SqlProfiler.ParameterFormat(sql, pars));
#if DEBUG
               Console.WriteLine(SqlProfiler.ParameterFormat(sql, pars));
#endif
           };
         });
    }

}
