

using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using FastTunnel.WebClient;
using FastTunnel.WebClient.Extension;

var builder = WebApplication.CreateBuilder(args).Inject();
builder.Host.UseSerilogDefault();
builder.WebHost.UseUrls("http://*:1271");
builder.Host.ConfigureServices((hostContext, services) =>
{
    services.AddJwt<JwtHandler>(enableGlobalAuthorize: true);
    services.AddCorsAccessor();
    SqlSugarConfigure(services);
    services.AddFastTunnelWebClient(hostContext.Configuration.GetSection("ClientSettings"));
    services.AddHostedService<WebClientInit>();
    //services.AddFastTunnelClient(hostContext.Configuration.GetSection("ClientSettings"));
});

builder.Services.AddControllers().AddInjectWithUnifyResult<XnRestfulResultProvider>().AddFriendlyException()
       .AddJsonOptions(options =>
       {
           //options.JsonSerializerOptions.DefaultBufferSize = 10_0000;//返回较大数据数据序列化时会截断，原因：默认缓冲区大小（以字节为单位）为16384。
           options.JsonSerializerOptions.Converters.AddDateFormatString("yyyy-MM-dd HH:mm:ss");
           options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
           //options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles; // 忽略循环引用 仅.NET 6支持
       });

// Add services to the container.


var app = builder.Build();

app.UseCorsAccessor();
app.UseAuthentication();
app.UseAuthorization();
app.UseInject(String.Empty);
app.MapControllers();
app.Run();

/// <summary>
/// 配置sqlsuagr
/// </summary>
void SqlSugarConfigure(IServiceCollection services)
{
    #region 配置sqlsuagr
    DbType dbType = DbType.Sqlite;//这里修改数据库类型
    var config = new ConnectionConfig
    {
        ConnectionString = $"{App.Configuration[$"ConnectionStrings:{dbType.ToString()}"]}",
        DbType = dbType,
        IsAutoCloseConnection = true,
        InitKeyType = InitKeyType.Attribute
    };
    services.AddSqlSugar(config
        , db =>
        {
            db.Aop.OnLogExecuting = (sql, pars) =>
            {
                Console.WriteLine(SqlProfiler.ParameterFormat(sql, pars));
            };
        });
    #endregion
}
