// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using FastTunnel.Core.Config;
using Furion.DataEncryption;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace FastTunnel.WebClient.Service;

public class AuthService : IDynamicApiController
{


    public WebManager WebManager { get; protected set; }
    public AuthService(IOptionsMonitor<DefaultClientConfig> configuration)
    {
        WebManager = configuration.CurrentValue.WebManager;
    }


    /// <summary>
    /// 登录
    /// </summary>
    /// <returns></returns>
    [HttpPost("/user/login")]
    [AllowAnonymous]
    public dynamic Login(LoginInput input)
    {
        if (input.UserName == WebManager.UserName && input.Password == WebManager.PassWord)
        {
            // 生成 token
            var accessToken = JWTEncryption.Encrypt(new Dictionary<string, object>()
            {
                { "UserName", WebManager.UserName },  // 存储Id
                { "Password",WebManager.PassWord }, // 存储用户名
            });
            return new { UserName = WebManager.UserName, Token = accessToken };
        }
        else
        {
            throw Oops.Oh($"用户密码错误");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    [HttpPost("/user/logout")]
    [AllowAnonymous]
    public void LoginOut()
    {

    }
}
