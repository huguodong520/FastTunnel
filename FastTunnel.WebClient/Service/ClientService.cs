// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System.Net.WebSockets;
using FastTunnel.Core.Config;
using FastTunnel.Core.Extensions;
using FastTunnel.Core.Models;
using FastTunnel.Core.Utilitys;
using FastTunnel.Core.WebClientSocket;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace FastTunnel.WebClient.Service;

public class ClientService : IDynamicApiController
{
    private readonly ILogger<ClientService> _logger;
    private readonly IFastClientSocket _clientSocket;
    private readonly IMemoryCache _memoryCache;

    public DefaultClientConfig ClientConfig { get; private set; }

    public SuiDaoServer Server { get; protected set; }

    private readonly SqlSugar.SqlSugarScope _db;
    //public ClientService(ILogger<ClientService> logger, IOptionsMonitor<DefaultClientConfig> configuration)
    //{
    //    this._logger = logger;
    //    ClientConfig = configuration.CurrentValue;
    //    Server = ClientConfig.Server;
    //    _db = DbContext.Db;
    //}

    public ClientService(
        ILogger<ClientService> logger,
        IFastClientSocket clientSocket,
        IMemoryCache memoryCache,
        IOptionsMonitor<DefaultClientConfig> configuration)
    {
        this._logger = logger;
        this._clientSocket = clientSocket;
        this._memoryCache = memoryCache;
        ClientConfig = configuration.CurrentValue;
        Server = ClientConfig.Server;
        _db = DbContext.Db;
    }


    /// <summary>
    /// 分页查询
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/client/page")]
    public dynamic Page([FromQuery] ClientInput input)
    {
        #region
        //var data = new { title = "测试标题", author = "测试", pageviews = "1231", status = "published", display_time = DateTime.Now };

        //var data1 = new Client
        //{
        //    Name = "内网穿透内网穿透",
        //    ConfigType = 1,
        //    IsOpen = true,
        //    LocalIp = "192.168.0.32",
        //    LocalPort = 1271,
        //    ServerPort = 1274,
        //    UpdatedTime = DateTime.Now,
        //    Protocol = "TCP",
        //    Result = "192.168.0.32:1274",
        //    Remark = "这是备注!"

        //};
        //var data2 = new Client
        //{
        //    Name = "内网穿透内网穿透",
        //    ConfigType = 1,
        //    IsOpen = false,
        //    LocalIp = "192.168.0.33",
        //    LocalPort = 1271,
        //    ServerPort = 1274,
        //    UpdatedTime = DateTime.Now,
        //    Protocol = "TCP",
        //    Result = "192.168.0.32:1274",
        //    Remark = "这是备注!"

        //};
        //var dataList = new List<Client>() { data1, data2 };
        //return new { PageIndex = 1, PageSize = 10, Total = 1, Rows = dataList };
        #endregion
        int total = 0;
        //int totalPage = 0;
        var result = _db.Queryable<Client>().ToPageList(input.PageIndex, input.PageSize, ref total);
        return new { PageIndex = input.PageIndex, PageSize = input.PageSize, Total = total, Rows = result };
    }


    /// <summary>
    /// 添加
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/client/add")]
    public async Task Add(ClientAddInput input)
    {
        if (input.RemotePort == Server.ServerPort)
        {
            throw Oops.Oh($"服务端端口{input.RemotePort }不可用");
        }
        var webPort = App.Configuration["WebConfig:Port"];
        if (input.LocalPort.ToString() == webPort)
        {
            throw Oops.Oh($"内网端口{input.RemotePort }不可用");
        }
        var entity = input.Adapt<Client>();
        await CheckInput(entity);
        if (entity.ConfigType == 2)
        {
            entity.RemotePort = Server.ServerPort;
        }
        ///发socket数据
        await SendMassage(entity);
        #region 缓存中拿结果
        ///缓存中拿结果
        var key = $"{input.LocalIp}:{input.LocalPort}:{input.ConfigType}:{(int)OperationEnum.Add}";
        var message = GetMessage(key);
        #endregion
        if (message != null)//判断是否为空
        {
            entity.Result = message.Result;
            await _db.Insertable(entity).IgnoreColumns(true).ExecuteCommandAsync();
        }
        else
        {
            throw Oops.Oh($"添加失败");
        }

    }

    /// <summary>
    /// 更新
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/client/update")]
    public async Task Update(ClientUpdateInput input)
    {
        var exist = await _db.Queryable<Client>().FirstAsync(it => it.Id == input.Id);
        if (exist == null) throw Oops.Oh($"数据不存在");

        var entity = input.Adapt<Client>();
        entity.UpdatedTime = DateTime.Now;

        if (entity.ConfigType != exist.ConfigType || entity.RemotePort != exist.RemotePort || entity.LocalIp != exist.LocalIp || entity.LocalPort != exist.LocalPort || entity.SubDomain != exist.SubDomain || entity.Protocol != exist.Protocol)
        {
            await CheckInput(entity);

            ///发socket数据
            await SendMassage(exist, true);
            ///发socket数据
            await SendMassage(entity);
            ///缓存中拿结果
            var key = $"{entity.LocalIp}:{entity.LocalPort}:{entity.ConfigType}:{(int)OperationEnum.Add}";
            var message = GetMessage(key);
            if (message != null)//判断是否为空
            {
                entity.Result = message.Result;

                await _db.Updateable(entity).ExecuteCommandAsync();
            }
            else
            {
                throw Oops.Oh($"添加失败");
            }
        }
        else
        {
            await _db.Updateable(entity).ExecuteCommandAsync();

        }

    }

    /// <summary>
    ///删除
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/client/delete")]
    public async Task Delete(ClientDeleteInput input)
    {
        var exist = await _db.Queryable<Client>().FirstAsync(it => it.Id == input.Id);
        if (exist == null) throw Oops.Oh($"数据不存在");
        ///发socket数据
        await SendMassage(exist, true);
        ///缓存中拿结果
        var key = $"{exist.LocalIp}:{exist.LocalPort}:{exist.ConfigType}:{(int)OperationEnum.Delete}";
        var message = GetMessage(key);
        if (message != null)//判断是否为空
        {

            await _db.Deleteable<Client>().Where(it => it.Id == input.Id).ExecuteCommandAsync();
        }
        else
        {
            throw Oops.Oh($"添加失败");
        }
    }

    /// <summary>
    /// 启用/停用
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/client/isopen")]
    public async Task IsOpen(ClientIsOpenInput input)
    {
        var exist = await _db.Queryable<Client>().FirstAsync(it => it.Id == input.Id);
        if (exist == null) throw Oops.Oh($"数据不存在");

        exist.IsOpen = input.IsOpen.Value;
        exist.UpdatedTime = DateTime.Now;
        ///发socket数据
        await SendMassage(exist, !exist.IsOpen);
        var opeation = exist.IsOpen ? (int)OperationEnum.Add : (int)OperationEnum.Delete;
        ///缓存中拿结果
        var key = $"{exist.LocalIp}:{exist.LocalPort}:{exist.ConfigType}:{opeation}";
        var message = GetMessage(key);
        if (message != null)//判断是否为空
        {
            await _db.Updateable<Client>(exist).ExecuteCommandAsync();
        }
        else
        {
            throw Oops.Oh($"操作失败");
        }

    }

    /// <summary>
    /// 获取socket消息
    /// </summary>
    /// <param name="client"></param>
    /// <param name="isDelete"></param>
    /// <returns></returns>
    [NonAction]
    public async Task SendMassage(Client client, bool isDelete = false)
    {
        var forwards = new List<ForwardConfig>();
        var webs = new List<WebConfig>();
        switch (client.ConfigType)
        {
            case 1:
                var forward = client.Adapt<ForwardConfig>();
                if (isDelete) forward.IsDelete = true;
                forwards.Add(forward);
                break;
            case 2:
                var web = client.Adapt<WebConfig>();
                if (isDelete) web.IsDelete = true;
                webs.Add(web);
                break;
        }
        var login = new LogInMassage
        {
            IsUpdate = true,
            Webs = webs,
            Forwards = forwards,
        };
        var msg = JsonConvert.SerializeObject(login);
        await _clientSocket.SendCmdAsync(Core.Models.MessageType.LogIn, msg, CancellationToken.None);


    }

    /// <summary>
    /// 获取socket消息
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    [NonAction]
    public CallBackMessage GetMessage(string key)
    {
        Thread.Sleep(1000);
        CallBackMessage? message = null;
        int count = 0;
        while (count < 3)
        {
            var cache = _memoryCache.Get<CallBackMessage>(key);
            if (cache != null)
            {
                message = cache;
                break;
            }
            else
            {
                Thread.Sleep(200);
                count++;
            }
        }
        return message;
    }

    /// <summary>
    /// 检查输入冲突
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [NonAction]
    public async Task CheckInput(Client input)
    {
        #region
        var exp = Expressionable.Create<Client>();
        exp.Or(it => it.LocalIp == input.LocalIp && it.LocalPort == input.LocalPort);//拼接OR
        exp.Or(it => it.RemotePort == input.RemotePort);//拼接OR
        var exist = await _db.Queryable<Client>().WhereIF(input.Id > 0, it => it.Id != input.Id).Where(exp.ToExpression()).AnyAsync();
        if (exist)
        {
            throw Oops.Oh($"内网端口/服务端端口和已存在配置中的端口冲突");
        }
        #endregion

    }
}
