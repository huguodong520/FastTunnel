// Licensed under the Apache License, Version 2.0 (the "License").
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://github.com/FastTunnel/FastTunnel/edit/v2/LICENSE
// Copyright (c) 2019 Gui.H

using System.ComponentModel.DataAnnotations;
using FastTunnel.Core.Models;
using Furion.DataValidation;

namespace FastTunnel.WebClient.Service;

public class ClientDto
{

}

public class ClientInput
{
    /// <summary>
    /// 页数
    /// </summary>
    public int PageIndex { get; set; } = 1;

    /// <summary>
    /// 每页数量
    /// </summary>
    public int PageSize { get; set; } = 10;
}

public class ClientAddInput
{
    /// <summary>
    /// 配置名称 
    ///</summary>
    [Required(ErrorMessage = "配置名称必填")]
    public string Name { get; set; }

    /// <summary>
    /// 配置类型 
    ///</summary>
    public int ConfigType { get; set; } = 1;

    /// <summary>
    /// 内网IP 
    ///</summary>
    [Required(ErrorMessage = "内网IP地址必填")]
    [DataValidation(ValidationTypes.IPv4, ErrorMessage = "Ip地址错误")]
    public string LocalIp { get; set; }

    /// <summary>
    /// 内网端口 
    ///</summary>
    [Range(1, 65535, ErrorMessage = "端口范围为1-65535")]

    public int LocalPort { get; set; }

    /// <summary>
    /// 服务端端口 
    ///</summary>
    [Range(1, 65535, ErrorMessage = "端口范围为1-65535")]
    public int RemotePort { get; set; }

    /// <summary>
    /// 通讯协议 
    ///</summary>
    public ProtocolEnum Protocol { get; set; } = ProtocolEnum.TCP;

    /// <summary>
    /// 子域名 
    ///</summary>
    public string SubDomain { get; set; }

    /// <summary>
    /// 生成的结果字符串预览 
    ///</summary>
    public string Result { get; set; }

    /// <summary>
    /// 是否启用 
    ///</summary>
    public bool IsOpen { get; set; } = true;

    /// <summary>
    /// 更新时间 
    ///</summary>
    public DateTime UpdatedTime { get; set; } = DateTime.Now;

    /// <summary>
    /// 备注
    /// </summary>
    public string Remark { get; set; }

}

public class ClientUpdateInput : ClientAddInput
{
    /// <summary>
    /// 
    /// </summary>
    [Required(ErrorMessage = "Id必填")]
    public int? Id { get; set; }
}

public class ClientDeleteInput
{
    /// <summary>
    /// 
    /// </summary>
    [Required(ErrorMessage = "Id必填")]
    public int? Id { get; set; }
}

public class ClientIsOpenInput : ClientDeleteInput
{
    /// <summary>
    /// 是否启用
    /// </summary>
    [Required(ErrorMessage = "是否启用必填")]
    public bool? IsOpen { get; set; }
}
