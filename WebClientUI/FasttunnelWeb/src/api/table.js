import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/client/page',
    method: 'get',
    params
  })
}

export function clientAdd(data) {
  return request({
    url: '/client/add',
    method: 'post',
    data
  })
}

export function clientUpdate(data) {
  return request({
    url: '/client/update',
    method: 'post',
    data
  })
}

export function clientDelete(data) {
  return request({
    url: '/client/delete',
    method: 'post',
    data
  })
}

export function clientStatus(data) {
  return request({
    url: '/client/isopen',
    method: 'post',
    data
  })
}
